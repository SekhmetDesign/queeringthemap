from django.db import migrations, models
from django.contrib.gis.geos import Point

def populate_moments(apps, schema_editor):
    Moment = apps.get_model("pinprick", "Moment")
    Moment.objects.bulk_create([
        Moment(status = "manual_ham", description = "Are You Leaving?", location = Point(-73.61449107527733, 45.526081999114176)),
        Moment(status = "manual_ham", description = "A bright day in summer, my friend gave me her old pink slip. I wore it proudly in the middle of the parc, the sun and dirty looks nourishing my faggoty femme self", location = Point(-73.588321, 45.532569)),
        Moment(status = "manual_ham", description = "I pushed you away with unfettered anger when you told me you were attracted to masc guys, my femme insecurity pulsating", location = Point(-73.582223, 45.515523)),
        Moment(status = "manual_ham", description = "We made out for 20 minutes in the middle of the sidealk while mutiple people wove around us without saying a word. You tasted like barbeque sauce", location = Point(-73.617726, 45.531916)),
        Moment(status = "manual_ham", description = "A lack of definition, of trying to be ~chill~, landed us in an argument when you thought i was flirting with someone else, which I was only doing becasue I thought I may as well try if I was going to place within the ~chill~ olympics of the queerer than thou", location = Point(-73.614786, 45.525921)),
        Moment(status = "manual_ham", description = "Location of the Sex Garage party on Sunday, July 15, 1990, when it was raided by police officers. The police became violent and the patrons fought back aggressively, this mass revolt leading to it becing referred to as 'Montreals Stonewall'", location = Point(-73.563735, 45.502508)),
        Moment(status = "manual_ham", description = "exploring our queer history, an unispiring foursome with two daddies. we kissed, recollected, and reconnected on the sweat drenched sex swing", location = Point(-73.591841, 45.522538)),
        Moment(status = "manual_ham", description = "biked for an hour to have hook up with a guy. on the way back it rained and i was soaked and ecstatic. too nervous to tell my roomates about my slutty escapade, I channeled all of its ecstatic affect into the feeling of biking in the pouring rain, effectively erasing the queerness in my experience", location = Point(-73.591897, 45.566017))
    ])

class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('pinprick', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(populate_moments),
    ]
