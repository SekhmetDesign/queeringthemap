from django.contrib import admin
from django.contrib.gis.admin import OSMGeoAdmin
from .models import Moment


@admin.register(Moment)
class MomentAdmin(OSMGeoAdmin):
    search_fields = ('description', )
