from json_views.views import PaginatedJSONListView
from django.views.generic.edit import BaseCreateView
from django.http import JsonResponse
from django.contrib.gis.measure import D

from .models import Moment, mkpoint
from .forms import SubmitMomentForm


class MomentView(PaginatedJSONListView):
    model = Moment

    paginate_by = 20

    def get_distance(self):
        return 5000  # 5km

    def get_queryset(self):
        qs = super().get_queryset().filter(status='manual_ham')
        if 'lat' in self.request.GET and 'lng' in self.request.GET:
            lat = float(self.request.GET['lat'])
            lng = float(self.request.GET['lng'])
            qs = qs.filter(location__distance_lte=(
                mkpoint(lat, lng),
                D(m=self.get_distance())
            ))
        return qs


class SubmitMomentView(BaseCreateView):
    form_class = SubmitMomentForm

    success_url = '/'  # Overriden

    def form_valid(self, form):
        super().form_valid(form)
        return JsonResponse({}, status=202)  # 202 Accepted

    def form_invalid(self, form):
        super().form_invalid(form)
        return JsonResponse(form.errors.get_json_data(), status=400)  # 400 Bad Request

    def render_to_response(self, context):
        pass
