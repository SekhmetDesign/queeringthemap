var map;
var theCurrentPos;
var theCurrentMarker ;
var defaultIcon, highlightedIcon, largeInfowindow;

var alerts = {
  post: {
    success: "Your story has been submitted to Queering The Map!",
    error: "Your story was not successfully added. Please try again."
  },
  load: {
    success: "Moments loaded",
    error: "Moments failed to load"
  }
};

// var center = localStorage.getItem('location') !== null ? JSON.parse(localStorage.getItem('location')) : { lat: 45.501689,  lng: -73.567256 };

var center = { lat: 45.501689,  lng: -73.567256 };

var pinned_moments = [];

function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

function makeNotification(text) {
  new Noty({
    timeout: 1000,
    text: text,
  }).show();
}

function populateInfoWindow(marker, infoWindow) {
  var markerDiv = document.createElement('div');
  var markerText = document.createTextNode(marker.title);
  markerDiv.appendChild(markerText);
  infoWindow.setContent(markerDiv);
  infoWindow.open(map, marker);
}

function makeMarkerIcon(markerColor) {
  var mq = window.matchMedia('(max-width: 800px)');
  var markerImage;

  if (mq.matches) {
    markerImage = new google.maps.MarkerImage(
    'http://chart.googleapis.com/chart?chst=d_map_spin&chld=1.15|0|' + markerColor +
    '|40|_|%E2%80%A2',
    new google.maps.Size(42, 68),
    new google.maps.Point(0, 0),
    new google.maps.Point(10, 34),
    new google.maps.Size(42, 68));
  }
  else {
    markerImage = new google.maps.MarkerImage(
    'http://chart.googleapis.com/chart?chst=d_map_spin&chld=1.15|0|' + markerColor +
    '|40|_|%E2%80%A2',
    new google.maps.Size(21, 34),
    new google.maps.Point(0, 0),
    new google.maps.Point(10, 34),
    new google.maps.Size(21, 34));
  }

  return markerImage;
}

var styles =[
  {
    featureType: 'administrative',
    elementType: 'labels',
    stylers: [ {hue: '#edbbd2'}, {lightness: -20}, {saturation: 50}, {gamma: 1.0} ]
  },
  {
    featureType: 'administrative.neighborhood',
    elementType: 'labels',
    stylers: [ {visibility: 'off'} ]
  },
  {
    featureType: 'poi',
    elementType: 'labels',
    stylers: [ { hue: '#edbbd2' }, { lightness: -20 }, { saturation: 50 }, { gamma: 1.0 }, { visibility: 'off' } ]
  },
  {
    featureType: 'poi',
    elementType: 'geometry',
    stylers: [ { hue: '#d1a6bf' }, { lightness: -9 }, { saturation: 30 }, { gamma: 1.0 } ]
  },
  {
    featureType: 'poi.park',
    elementType: 'geometry',
    stylers: [ { hue: '#b5a891' }, { lightness: -3 }, { saturation: -50 }, { gamma: 1.0 } ]
  },
  {
    featureType: 'poi.business',
    elementType: 'geometry',
    stylers: [ { hue: '#d2a6be' }, { lightness: 4 }, { saturation: -5 }, { gamma: 1.0 } ]
  },
  {
    featureType: 'poi.school',
    elementType: 'geometry',
    stylers: [ { hue: '#dea8a0' }, { lightness: 17 }, { saturation: -20 }, { gamma: 1.0 } ]
  },
  {
    featureType: 'poi.medical',
    elementType: 'geometry',
    stylers: [ { hue: '#e49db6' }, { lightness: 2 }, { saturation: 10 }, { gamma: 1.0 } ]
  },
  {
    featureType: 'road',
    elementType: 'geometry',
    stylers: [ { hue: '#ff9c40' }, { saturation: 0 }, { lightness: 0 } ]
  },
  {
    featureType: 'road.arterial',
    elementType: 'geometry',
    stylers: [ { hue: '#ffca81' }, { saturation: 0 }, { lightness: 0 } ]
  },
  {
    featureType: 'road.local',
    elementType: 'geometry',
    stylers: [ { hue: '#ff99dd' }, { saturation: 50 }, { lightness: -10 }, { gamma: 1.0 } ]
  },
  {
    featureType: 'road',
    elementType: 'labels',
    stylers: [ { hue: '#ff9c40' }, { lightness: 0 }, { saturation: 0 }, { gamma: 1.0 } ]
  },
  {
    featureType: 'road.highway',
    elementType: 'labels',
    stylers: [ { visibility: 'off' } ]
  },
  {
    featureType: 'road.arterial',
    elementType: 'labels',
    stylers: [ { hue: '#ffca81' }, { lightness: 0 }, { saturation: 0 }, { gamma: 1.0 } ]
  },
  {
    featureType: 'road.local',
    elementType: 'labels',
    stylers: [ { hue: '#ff99dd' }, { saturation: 50 }, { lightness: -10 }, { gamma: 1.0 } ]
  },
  {
    featureType: 'landscape',
    elementType: 'geometry',
    stylers: [ { hue: '#edbbd3' }, { lightness: -9 }, { saturation: 50 }, { gamma: 1.0 } ]
  },
  {
    featureType: 'landscape.man_made',
    elementType: 'geometry',
    stylers: [ { hue: '#edbbd2' }, { lightness: 0 }, { saturation: 0 }, { gamma: 1.0 } ]
  },
  {
    featureType: 'landscape.man_made',
    elementType: 'labels',
    stylers: [ { hue: '#edbbd2' }, { lightness: -20 }, { saturation: 50 }, { gamma: 1.0 }, { visibility: 'off' } ]
  },
  {
    featureType: 'landscape.natural',
    elementType: 'geometry',
    stylers: [ { hue: '#f3bfd9' }, { lightness: 50 }, { saturation: 80 }, { gamma: 1.0 } ]
  },
  {
    featureType: 'landscape.natural',
    elementType: 'labels',
    stylers: [ { hue: '#f3bfd9' }, { lightness: -20 }, { saturation: 50 }, { gamma: 1.0 }, { visibility: 'off' } ]
  },
  {
    featureType: 'transit.line',
    elementType: 'all',
    stylers: [ { visibility: 'off' } ]
  },
  {
    featureType: 'transit',
    elementType: 'labels',
    stylers: [ { hue: '#edbbd2' }, { lightness: -20 }, { saturation: 50 }, { gamma: 1.0 } ]
  },
  {
    featureType: 'water',
    elementType: 'geometry',
    stylers: [ { hue: '#BEC2FF' }, { lightness: -5 }, { saturation: 0 }, { gamma: 1.0 }, { visibility: 'simplified' } ]
  },
  {
    featureType: 'water',
    elementType: 'labels',
    stylers: [ { hue: '#BEC2FF' }, { lightness: -33 }, { saturation: 40 }, { gamma: 1.0 } ]
  }
];

function saveMoment(captcha) {
  var name_element = document.getElementById('txt_contents');

  console.log(captcha);

  $.ajax({
    url: '/submit',
    data: {
      'latitude': theCurrentPos.lat(),
      'longitude': theCurrentPos.lng(),
      'description': name_element.value,
      'captcha': captcha,
    },
    type: 'POST',
    headers: {
      'X-CSRFToken': getCookie('csrftoken'),
    }
  }).done(function(data) {
    var largeInfoWindow = new google.maps.InfoWindow();
    var bounds = new google.maps.LatLngBounds();
    var defaultIcon = makeMarkerIcon('000000');
    var highlightedIcon = makeMarkerIcon('FF00C1');

    var marker = new google.maps.Marker({
      position: theCurrentPos,
      title: name_element.value,
      map: map,
      icon: highlightedIcon,
    });

    document.querySelector("#add").classList.toggle('hidden');

    name_element.value = '';
    theCurrentMarker.setMap(null);
    bounds.extend(marker.position);

    marker.addListener('click', function() {
      populateInfoWindow(this, largeInfoWindow);
    });
    marker.addListener('mouseover', function() {
      this.setIcon(highlightedIcon);
    });
    marker.addListener('mouseout', function() {
      this.setIcon(defaultIcon);
    });

    makeNotification(alerts.post.success);

  }).fail(function(data) {
    makeNotification(alerts.post.error);
  });

  toggleRightNav()
  grecaptcha.reset();
}
window.saveMoment = saveMoment;

function initMap(callback) {

  // // Try HTML5 geolocation.
  // if (navigator.geolocation) {
  //   navigator.geolocation.getCurrentPosition(function(position) {
  //   var pos = {
  //     lat: position.coords.latitude,
  //     lng: position.coords.longitude
  //   };

  //   // localStorage.setItem('location', JSON.stringify(pos));
    
  //   map.setCenter(pos);
  // }, function() {
  //   handleLocationError(true, infoWindow, map.getCenter());

  // });
  // } else {
  //   // Browser doesn't support Geolocation
  //   handleLocationError(false, infoWindow, map.getCenter());
  // }

  map = new google.maps.Map(document.getElementById('map'), {
    center: center,
    zoom: 13,
    maxZoom: 18,
    minZoom: 3,
    styles: styles,
    mapTypeControl: false
  });

  infoWindow = new google.maps.InfoWindow;

  map.addListener('click', function(event) {
    placeMarker(event.latLng);
  });

  // map.addListener('dragend', function(event) {
  //   center.lat = map.getCenter().lat();
  //   center.lng = map.getCenter().lng();
  //   retrieveFromBackend();
  // });

  var icon = {
    url: '/static/images/addmarker.png',
    scaledSize: new google.maps.Size(25, 40)
  };

  function placeMarker(location) {
    if (windowOpen === true) {
      theCurrentMarker.setPosition(location);
      theCurrentPos = location;
    }
    else {
      if (theCurrentMarker !== undefined) {
        theCurrentMarker.setMap(null);
      }
      theCurrentMarker = new google.maps.Marker({
        position: location,
        map: map,
        icon: icon,
      });
      theCurrentPos = location;
    }
  }

  largeInfowindow = new google.maps.InfoWindow();
  var bounds = new google.maps.LatLngBounds();
  defaultIcon = makeMarkerIcon('000000');
  highlightedIcon = makeMarkerIcon('FF00C1');

  callback();
}


function retrieveFromBackend() {

  $.getJSON('/moments', {
    // lat: center.lat,
    // lng: center.lng
  }, function(result) {
    if (result.is_paginated) {

      for (var page = result.current + 1; page <= result.pages; page++) {

        $.getJSON('/moments', {
          page: page
          // lat: center.lat,
          // lng: center.lng
        }, function(result) {
          result.moment_list.forEach(function (moment) {
            if (!(pinned_moments.indexOf(moment.id) + 1)) {
              var marker = new google.maps.Marker({
                map: map,
                position: {
                  'lat': moment.latitude,
                  'lng': moment.longitude
                },
                title: moment.description,
                icon: defaultIcon,
                animation: null
              });

              marker.addListener('click', function() {
                populateInfoWindow(this, largeInfowindow);
              });
              marker.addListener('mouseover', function() {
                this.setIcon(highlightedIcon);
              });
              marker.addListener('mouseout', function() {
                this.setIcon(defaultIcon);
              });

              pinned_moments.push(moment.id);
            }
          });

          // makeNotification(MOMENTS_LOADED_TEXT);
        });
      }
    }
    
    result.moment_list.forEach(function (moment) {
      if (!(pinned_moments.indexOf(moment.id) + 1)) {
        var marker = new google.maps.Marker({
          map: map,
          position: {
            'lat': moment.latitude,
            'lng': moment.longitude
          },
          title: moment.description,
          icon: defaultIcon,
          animation: google.maps.Animation.DROP,
        });

        marker.addListener('click', function() {
          populateInfoWindow(this, largeInfowindow);
        });
        marker.addListener('mouseover', function() {
          this.setIcon(highlightedIcon);
        });
        marker.addListener('mouseout', function() {
          this.setIcon(defaultIcon);
        });

        pinned_moments.push(moment.id);
      }
    });

    // makeNotification(MOMENTS_LOADED_TEXT);
  })
  .fail(function() {
    console.error('Error - failed to retrieve data');
  });
}

function init() {
  initMap(retrieveFromBackend);
};